using UnityEngine;
using System.Collections.Generic;

namespace Lean.Touch
{


	// This script will hook into event LeanTouch event, and spam the console with the information
	public class LeanTouchEvents : MonoBehaviour
	{
		[Header("Variable")]
		public Vector3 lasPos;
		public bool noMorePause = false;

		[Header("CoreEngine variable")]
		public GameObject coreEngine ;

		void Update(){
			//checkFingersCount ();
		}

		// Get Set method
		private void alterPause(){
			GameObject[] rs = GameObject.FindGameObjectsWithTag ("rs");

				GameObject ov = (GameObject)Instantiate (Resources.Load ("RePoint"));
				ov.transform.position = lasPos;
			
		}

		void OnApplicationPause(bool pauseStatus){
			if(pauseStatus == true){
				coreEngine.GetComponent<CoreGame> ().state = "pause";
				noMorePause = true;
				alterPause ();
			}
		}

		private void checkFingersCount(){
			if(coreEngine.GetComponent<CoreGame>().state == "playing"){
				if(LeanTouch.Fingers.Count <= 0){
					GameObject ov = (GameObject)Instantiate(Resources.Load("EndPoint"));
					ov.transform.position = lasPos;
					coreEngine.GetComponent<CoreGame> ().setOver ();
				}
			}
		}
		//
		protected virtual void OnEnable()
		{
			// Hook into the events we need
			LeanTouch.OnFingerDown  += OnFingerDown;
			LeanTouch.OnFingerSet   += OnFingerSet;
			LeanTouch.OnFingerUp    += OnFingerUp;
			LeanTouch.OnFingerTap   += OnFingerTap;
			LeanTouch.OnFingerSwipe += OnFingerSwipe;
			LeanTouch.OnGesture     += OnGesture;
		}
		
		protected virtual void OnDisable()
		{
			// Unhook the events
			LeanTouch.OnFingerDown  -= OnFingerDown;
			LeanTouch.OnFingerSet   -= OnFingerSet;
			LeanTouch.OnFingerUp    -= OnFingerUp;
			LeanTouch.OnFingerTap   -= OnFingerTap;
			LeanTouch.OnFingerSwipe -= OnFingerSwipe;
			LeanTouch.OnGesture     -= OnGesture;
		}
		
		public void OnFingerDown(LeanFinger finger)
		{
			if(coreEngine.GetComponent<CoreGame>().state == "idle"){
				coreEngine.GetComponent<CoreGame> ().setPlaying ();
			}else if(coreEngine.GetComponent<CoreGame>().state == "pause"){   // resume check

				Ray ray = finger.GetRay ();
				RaycastHit hit = default(RaycastHit);

				if (Physics.Raycast (ray, out hit, float.PositiveInfinity) == true) {
					if (hit.collider.name.Contains ("RePoint")) {
						coreEngine.GetComponent<CoreGame> ().setPlaying ();
						coreEngine.GetComponent<CoreGame> ().clearAfterPause ();

						noMorePause = false;
					}
				} else {
					noMorePause = true;
				}


			}


			if(coreEngine.GetComponent<CoreGame>().state == "idle" || coreEngine.GetComponent<CoreGame>().state == "playing"  ){
				if(finger.IsOverGui == false){
				GameObject pw = (GameObject)Instantiate (Resources.Load ("PointWave"));
				
				pw.transform.position = finger.GetWorldPosition (10);
				}
			}
		}

		public void OnFingerSet(LeanFinger finger)
		{
			Ray ray = finger.GetRay();
			RaycastHit hit = default(RaycastHit);

			// Was this finger pressed down on a collider?
			if (Physics.Raycast(ray, out hit, float.PositiveInfinity) == true) { //ADDED LAYER SELECTION
				//if (Physics.Raycast(ray, out hit) == true) { //OLD METHOD with no layer selection, which will only filter out IgnoreRaycast layer

				// Was that collider this one?
				if (hit.collider.name.Contains("Obstacle")&& coreEngine.GetComponent<CoreGame>().state != "over" && coreEngine.GetComponent<CoreGame>().state != "pause" )
				{
					// Set GameOver
					GameObject ov = (GameObject)Instantiate(Resources.Load("EndPoint"));
					ov.transform.position = finger.GetWorldPosition (5);
					coreEngine.GetComponent<CoreGame>().setOver();
				}
			}



		}
		
		public void OnFingerUp(LeanFinger finger)
		{
			
			if (coreEngine.GetComponent<CoreGame> ().state == "pause" && finger.IsOverGui == false && noMorePause == false) {
				GameObject ov = (GameObject)Instantiate(Resources.Load("RePoint"));
				ov.transform.position = finger.GetWorldPosition (5);
			} else {
				if(finger.IsOverGui == false){
				lasPos = finger.GetWorldPosition (10);
				}
			}
			//Debug.Log(LeanTouch.Fingers.Count);
			///Debug.Log("Finger " + finger.Index + " finished touching the screen");
		}
		
		public void OnFingerTap(LeanFinger finger)
		{
			///Debug.Log("Finger " + finger.Index + " tapped the screen");
		}
		
		public void OnFingerSwipe(LeanFinger finger)
		{
			///Debug.Log("Finger " + finger.Index + " swiped the screen");
		}
		
		public void OnGesture(List<LeanFinger> fingers)
		{
			/*
			Debug.Log("Gesture with " + fingers.Count + " finger(s)");
			Debug.Log("    pinch scale: " + LeanGesture.GetPinchScale(fingers));
			Debug.Log("    twist degrees: " + LeanGesture.GetTwistDegrees(fingers));
			Debug.Log("    twist radians: " + LeanGesture.GetTwistRadians(fingers));
			Debug.Log("    screen delta: " + LeanGesture.GetScreenDelta(fingers));
			*/
		}
	}
}