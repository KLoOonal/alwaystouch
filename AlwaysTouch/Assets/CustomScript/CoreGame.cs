﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class CoreGame : MonoBehaviour 
{

	[Header ("Core game variable")]
	public string state = "idle";
	//  1. idle 2.playing 3.over 4. Prepare 5.pauses
	[Header("Score Manager")]

	public float currentScore = 0;
	public int highScore = 0;
	public GameObject scoreUI;

	[Header("Interface")]
	public GameObject startUI;
	public GameObject playingUI;
	public GameObject overUI;
	public GameObject prepareUI;


	// Use this for initialization
	void Start ()
	{
			
	}
		
	// Update is called once per frame
	void Update ()
	{
		checkGameState ();
		checkScore ();
	}


	// Core method
	private void checkGameState ()
	{
		if (state == "idle") {
			currentScore = 0;
			prepareUI.SetActive (true);
			overUI.SetActive (false);
		} else if (state == "playing") {
			startUI.SetActive (false);
			prepareUI.SetActive (false);
			playingUI.SetActive (true);
		} else if (state == "over") {
			playingUI.SetActive (false);
			overUI.SetActive (true);

		}else if(state == "pause"){
			playingUI.transform.Find ("PauseText").gameObject.SetActive (true);
			playingUI.transform.Find ("PauseButton").gameObject.SetActive (false);
		}else if(state == "prepare"){
			
		}
	}

	private void checkScore(){
		if(state == "playing"){
			currentScore += Time.deltaTime;
			scoreUI.GetComponent<Text> ().text = ((int)(currentScore/3)).ToString ();
		}
	}


	// Set and Get method
	public void clearObject(){
		
		GameObject[] ed = GameObject.FindGameObjectsWithTag ("ed");

		foreach(GameObject edO in ed){
			Destroy (edO);
		}

		GameObject[] ob = GameObject.FindGameObjectsWithTag ("obs");
		foreach (GameObject obO in ob) {
			Destroy (obO);
		}
			
	}

	public void clearAfterPause(){
		GameObject[] rs = GameObject.FindGameObjectsWithTag ("rs");
		foreach (GameObject rsO in rs) {
			Destroy (rsO);
		}
		playingUI.transform.Find ("PauseText").gameObject.SetActive (false);
		playingUI.transform.Find ("PauseButton").gameObject.SetActive (true);
	}

	//
	public void setGameState (string str)
	{
		state = str;
	}

	public void pauseGame(){
		state = "pause";
	}

	public void unPauseGame(){
		playingUI.transform.Find ("PauseText").gameObject.SetActive (false);
		playingUI.transform.Find ("PauseButton").gameObject.SetActive (true);
		state = "playing";
	}

	public void retryGame(){
		state = "prepare";
	}

	public void setIdle(){
		state = "idle";
	}

	public void setPlaying(){
		state = "playing";
	}

	public void setOver(){
		state = "over";
	}

	// Test mentod

}
